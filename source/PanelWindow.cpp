#include "PanelWindow.h"

#include <Application.h>
#include <Message.h>
#include <stdio.h>
#include <Screen.h>

int32
pulse_thread_func( void * data )
{
	PanelWindow * win = (PanelWindow*)data;
	
	for (;;) 
	{
		uint64 pulse_rate = win->PulseRate();
		if ( !pulse_rate )
			pulse_rate = 100000;
		
		snooze( pulse_rate );
		win->Lock();
		{ // this ensures us that we won't leak any memory. I hope =)
			BMessage msg( PANEL_MODIFY_SIZE );
			win->PostMessage( &msg );
		}
		win->Unlock();
	}
}

PanelWindow::PanelWindow()
:	BWindow( BRect(200+0,0,200+35,10), "IconPanel", 
		B_NO_BORDER_WINDOW_LOOK,
		B_FLOATING_ALL_WINDOW_FEEL,
		B_NOT_MOVABLE|B_NOT_ZOOMABLE|B_NOT_RESIZABLE|B_ASYNCHRONOUS_CONTROLS|B_WILL_ACCEPT_FIRST_CLICK|B_AVOID_FOCUS|B_NOT_CLOSABLE
	),
	m_open( true ),
	m_size( 0 ),
	m_edge( TOP ),
	m_pulse_rate( 50000 ),
	m_small_icons( false ),
	m_thin_handle( false ),
	m_expand_on_mouse_over( false ),
	m_floating_window( true )
{
	SetWorkspaces( 0xffffffff );
	
	m_handle = new HandleView();
	m_handle->ResizeTo( Bounds().Width(), Bounds().Height() );
	AddChild( m_handle );
	
	AddChild( m_bubble_starter = new BubbleStarter() );
	
	CommonInit();
}

PanelWindow::PanelWindow( BMessage * msg )
:	BWindow( msg ),
	m_size( 0 )
{
	SetWorkspaces( 0xffffffff );
	
	for (; FindView("icon") ;)
	{ // remove views archived by BWindow, these are added again later on
		BView * view = FindView("icon");
		RemoveChild( view );
		delete view;
	}
	
	// get values
	int32 temp;
	msg->FindInt32("edge",&temp);
	m_edge = (ScreenEdge)temp;
	
	msg->FindBool( "open", &m_open );
	msg->FindInt64( "pulse_rate", &m_pulse_rate );
	msg->FindBool("small_icons",&m_small_icons);
	m_thin_handle = false;
	msg->FindBool("thin_handle", &m_thin_handle );
	msg->FindBool("expand_on_mouse_over",&m_expand_on_mouse_over);
	m_floating_window = true;
	msg->FindBool("floating_window",&m_floating_window);
	
	if ( m_floating_window )
		SetFeel(B_FLOATING_ALL_WINDOW_FEEL);
	else
		SetFeel(B_NORMAL_WINDOW_FEEL);
		SetFlags(Flags() | B_NOT_CLOSABLE | B_AVOID_FOCUS);
	
	if ( m_expand_on_mouse_over )
		m_open = false;
	
	BMessage view;
	
	// handle
	m_handle = dynamic_cast<HandleView*>( FindView("handle") );
	
	// icons
	for ( int32 c=0; msg->FindMessage("icon",c,&view) == B_OK; c++ )
	{
		m_icons += dynamic_cast<IconView*>( IconView::Instantiate(&view) );
		view.MakeEmpty();
	}
	
	if ( !FindView("bubble starter") )
		AddChild( m_bubble_starter = new BubbleStarter() );
	
	CommonInit();
}

PanelWindow::~PanelWindow()
{
	for (; ChildAt(0) ;)
		RemoveChild( ChildAt(0) );
	
	delete m_handle;
	
	m_icons.SetDeleteItems( true );
	
	suspend_thread( m_pulse_thread );
	kill_thread( m_pulse_thread );
}

void
PanelWindow::CommonInit()
{
	SetSizeLimits(1,5000,1,5000);
	
	if ( m_open )
	{
		m_size = m_icons.NumItems();
		for ( int32 c=0; c<m_size; c++ )
			AddChild( m_icons[c] );
	}
	
	m_pulse_thread = spawn_thread(
		pulse_thread_func,
		"Panel pulse thread",
		B_NORMAL_PRIORITY,
		this
	);
	resume_thread( m_pulse_thread );
	
	PositionViews();
	
	m_pos = Frame().LeftTop();
}

BArchivable *
PanelWindow::Instantiate( BMessage * msg )
{
	if ( !validate_instantiation(msg,"PanelWindow") )
		return NULL;
	
	return new PanelWindow(msg);
}

status_t
PanelWindow::Archive( BMessage * msg, bool deep ) const 
{
	msg->AddInt32("edge", m_edge);
	msg->AddBool("open", m_open);
	msg->AddInt64("pulse_rate", m_pulse_rate);
	msg->AddBool("small_icons",m_small_icons);
	msg->AddBool("thin_handle",m_thin_handle);
	msg->AddBool("expand_on_mouse_over",m_expand_on_mouse_over);
	msg->AddBool("floating_window",m_floating_window);
	
	for ( int c=0; m_icons[c]; c++ )
	{
		BMessage view;
		
		m_icons[c]->Archive(&view,true);
		msg->AddMessage("icon",&view);
	}
	
	return BWindow::Archive(msg,true);
}

bool
PanelWindow::QuitRequested()
{
	return true;
}

void
PanelWindow::ModifySize()
{
	if ( PulseRate() == 0 )
	{ // instant resize
		// add/remove an icon if needed
		if ( m_open )
		{
			while ( m_size < m_icons.NumItems() )
			{
				AddChild( m_icons[m_size] );
				m_size++;
			}
			PositionViews();
		} else
		{ // !m_open
			while ( m_size > 0 )
			{
				m_size--;
				RemoveChild( m_icons[m_size] );
			}
			PositionViews();
		}
		
		suspend_thread( m_pulse_thread ); // disable Pulse since we're done scrolling
		return;
	}
	
	// add/remove an icon if needed
	if ( m_open )
	{
		if ( m_size < m_icons.NumItems() )
		{
			AddChild( m_icons[m_size] );
			m_size++;
			
			PositionViews();
		} else
			suspend_thread( m_pulse_thread ); // disable Pulse since we're done scrolling
	} else
	{
		if ( m_size > 0 )
		{
			m_size--;
			RemoveChild( m_icons[m_size] );
			
			PositionViews();
		} else
			suspend_thread( m_pulse_thread ); // disable Pulse since we're done scrolling
	}
}

void
PanelWindow::PositionViews()
{
	BeginViewTransaction();

	float handle_height;
	float handle_width;
	float icon_width;
	float icon_height;
	
	float icon_size = 35;
	if ( m_small_icons )
		icon_size = 19;
	
	switch ( m_edge )
	{ // set up correct width/height for handle and icons
		case TOP:
		case BOTTOM:
			if ( m_thin_handle )
				handle_height = 1;
			else
				handle_height = 10;
			handle_width = icon_size;
			icon_width = icon_size;
			icon_height = icon_size+1;
			break;
		case LEFT:
		case RIGHT:
			handle_height = icon_size;
			if ( m_thin_handle )
				handle_width = 1;
			else
				handle_width = 10;
			icon_width = icon_size+1;
			icon_height = icon_size;
			break;
		default:
			EndViewTransaction(); // let's be kind to the app_server :)
			throw "Fatal error: Invalid screen edge in ModifySize()";
	}
	
	BRect bounds;
	
	BScreen scr( this );
	
	switch ( m_edge )
	{ // move views around
		case TOP:
			ResizeTo( handle_width, handle_height+icon_height*m_size );
			bounds = Bounds();
			
			m_handle->MoveTo(0,0);
			m_handle->ResizeTo( handle_width, handle_height );
			for ( int32 c=0; c<m_size; c++ )
			{
				m_icons[c]->ResizeTo( icon_size, icon_size );
				m_icons[c]->MoveTo(0,bounds.bottom-(icon_height*(c+1))+1);
			}
			break;
		case BOTTOM:
			ResizeTo( handle_width, handle_height+icon_height*m_size );
			MoveTo( Frame().left, scr.Frame().bottom - Bounds().Height() );
			bounds = Bounds();
			
			m_handle->MoveTo(0,bounds.bottom-handle_height);
			m_handle->ResizeTo( handle_width, handle_height );
			for ( int32 c=0; c<m_size; c++ )
			{
				m_icons[c]->ResizeTo( icon_size, icon_size );
				m_icons[c]->MoveTo(0,icon_height*c);
			}
			break;
		case LEFT:
			ResizeTo( handle_width + icon_width*m_size, handle_height );
			bounds = Bounds();
			
			m_handle->MoveTo(0,0);
			m_handle->ResizeTo( handle_width, handle_height );
			for ( int32 c=0; c<m_size; c++ )
			{
				m_icons[c]->ResizeTo( icon_size, icon_size );
				m_icons[c]->MoveTo(bounds.right-(icon_width*(c+1))+1,0);
			}
			break;
		case RIGHT:
			ResizeTo( handle_width + icon_width*m_size, handle_height );
			MoveTo( scr.Frame().right - Bounds().Width(), Frame().top );
			bounds = Bounds();
			
			m_handle->MoveTo(bounds.right-handle_width,0);
			m_handle->ResizeTo( handle_width, handle_height );
			for ( int32 c=0; c<m_size; c++ )
			{
				m_icons[c]->ResizeTo( icon_size, icon_size );
				m_icons[c]->MoveTo(icon_width*c,0);
			}
			break;
		default:
			EndViewTransaction(); // let's be kind to the app_server :)
			throw "Fatal error: Invalid screen edge in ModifySize()";
	}
	
	EndViewTransaction();
	
	UpdateIfNeeded();
}

void
PanelWindow::MessageReceived( BMessage * msg )
{
	switch ( msg->what )
	{
		case PANEL_SET_FLOATING:
		{
			msg->FindBool("floating_window",&m_floating_window);
			if ( m_floating_window )
				SetFeel(B_FLOATING_ALL_WINDOW_FEEL);
			else
				SetFeel(B_NORMAL_WINDOW_FEEL);
			be_app->PostMessage('Uset');
		}	break;
		
		case PANEL_SET_THIN_HANDLE:
		{
			msg->FindBool("thin_handle",&m_thin_handle);
			PositionViews();
			be_app->PostMessage('Uset');
		}	break;
		
		case PANEL_MOVE_ICON:
		{
			IconView * icon = NULL;
			int8 edge;
			
			int move = 0; // relative position
			
			msg->FindPointer("icon",(void**)&icon);
			msg->FindInt8("edge",&edge);
			
			switch ( m_edge )
			{
				case TOP:
					if ( edge == TOP )		move = 1;
					if ( edge == BOTTOM )	move = -1;
					break;
				case BOTTOM:
					if ( edge == TOP )		move = -1;
					if ( edge == BOTTOM )	move = 1;
					break;
				case LEFT:
					if ( edge == LEFT )		move = 1;
					if ( edge == RIGHT )	move = -1;
					break;
				case RIGHT:
					if ( edge == LEFT )		move = -1;
					if ( edge == RIGHT )	move = 1;
					break;
			}
			
			if ( !move )
			{
				printf("invalid move\n");
				break; // no valid move
			}
			
			int curr_pos = m_icons.IndexOf( icon );
			int new_pos = curr_pos + move;
			
			if ( new_pos < 0 || new_pos > m_icons.NumItems() )
				// stop if already at start or end and would move outside panel
				break;
			
			m_icons -= icon;
			m_icons.InsertAt( icon, new_pos );
			
			PositionViews();
			be_app->PostMessage('Uset');
		}	break;
		case PANEL_REMOVE_ICON:
		{
			IconView * icon = NULL;
			
			if ( msg->FindPointer("icon",(void**)&icon) == B_OK )
			{
				m_icons -= icon;
				
				if ( icon->Window() )
					RemoveChild( icon );
				
				if ( m_size > m_icons.NumItems() )
					m_size--;
				
				PositionViews();
				be_app->PostMessage('Uset');
				
				delete icon;
			}
			
		}	break;
		case PANEL_ADD_ICON:
		{
			PanelWindow * panel = NULL;
			
			msg->FindPointer("panel",(void**)&panel);
			
			if ( panel == this )
				break; // don't add drops from this window
			
			entry_ref ref;
			for ( int32 c=0; msg->FindRef("refs",c,&ref) == B_OK; c++ )
			{
				IconView * icon = new IconView( &ref );
				m_icons += icon;
				
				if ( m_open )
					AddChild( icon );
			}
			
			if ( m_open )
			{
				m_size = m_icons.NumItems();
				PositionViews();
			}
			be_app->PostMessage('Uset');
		}	break;
		case PANEL_MOVE_TO:
		{
			BPoint point;
			msg->FindPoint("pos",&point);
			
			BScreen scr( this );
			
			if( m_small_icons )
			{
				if ( point.x + 19 > scr.Frame().right )		// if icons are "small" this is the correct calculation
					point.x = scr.Frame().right - 19;
				if ( point.y + 19 > scr.Frame().bottom )
					point.y = scr.Frame().bottom - 19;
			}
			
			else
			{
				if ( point.x + 35 > scr.Frame().right )   // if icons "large", this is the way to go
					point.x = scr.Frame().right - 35;
				if ( point.y + 35 > scr.Frame().bottom )
					point.y = scr.Frame().bottom - 35;
			}
			
			
			switch ( m_edge )
			{
				case TOP:
					MoveTo( point.x, 0 );
					break;
				case BOTTOM:
					MoveTo( point.x, scr.Frame().bottom - Bounds().Height() );
					break;
				case LEFT:
					MoveTo( 0, point.y );
					break;
				case RIGHT:
					MoveTo( scr.Frame().right - Bounds().Width(), point.y );
					break;
			}
			
			m_pos = Frame().LeftTop();
		}	break;
		case PANEL_SET_EDGE:
		{
			int8 edge = 0;
			msg->FindInt8("edge",&edge);
			if ( m_edge != edge )
			{
				m_edge = (ScreenEdge)edge;
				PositionViews();
			
				m_handle->Invalidate();
				be_app->PostMessage('Uset');
			}
		}	break;
		case PANEL_TOGGLE_OPEN:
		{
			if ( !m_expand_on_mouse_over )
			{
				m_open = !m_open;
				if ( PulseRate() > 0 )
				{
					resume_thread( m_pulse_thread );
				} else
				{
					ModifySize();
				}
			}
		}	break;
		case PANEL_SET_OPEN:
		{
			int8 open = 0;
			msg->FindInt8("open",&open);
			m_open = (open > 0);
			resume_thread( m_pulse_thread );
		}	break;
		case PANEL_MODIFY_SIZE:
			ModifySize();
			break;
		case PANEL_SET_ICON_SIZE:
		{
			msg->FindBool("small_icons",&m_small_icons);
			PositionViews();
			be_app->PostMessage('Uset');
			for ( int32 c=0; ChildAt(c); c++ )
				ChildAt(c)->Invalidate();
		}	break;
		case PANEL_SET_SCROLL_RATE:
		{
			msg->FindInt64("scroll_rate",&m_pulse_rate);
			if ( m_pulse_rate == 0 )
			{
				suspend_thread( m_pulse_thread );
			}
			be_app->PostMessage('Uset');
		}	break;
		case PANEL_MOUSE_EXIT:
		{
			if ( m_expand_on_mouse_over && m_open)
			{
				m_open = false;
				resume_thread( m_pulse_thread );
			}
		}	break;
		case PANEL_MOUSE_ENTER:
		{
			if ( m_expand_on_mouse_over && !m_open)
			{
				m_open = true;
				resume_thread( m_pulse_thread );
			}
		}	break;
		case PANEL_SET_EXPAND_ON_MOUSE_OVER:
		{
			msg->FindBool("expand_on_mouse_over",&m_expand_on_mouse_over);
			be_app->PostMessage('Uset');
		}	break;
		default:
			BWindow::MessageReceived( msg );
	}
	
}

bigtime_t
PanelWindow::PulseRate()
{
	return m_pulse_rate;
}

void
PanelWindow::ScreenChanged( BRect screen, color_space )
{
	BRect frame = Frame();
	
	if ( frame.right > screen.right )
	{
		MoveTo( screen.right - frame.Width(), frame.top );
		frame = Frame();
	}
	
	if ( frame.bottom > screen.bottom )
	{
		MoveTo( frame.left, screen.bottom - frame.Height() );
		frame = Frame();
	}
	
	if ( m_edge == RIGHT )
	{
		MoveTo( screen.right - frame.Width(), frame.top );
		frame = Frame();
	}
	
	if ( m_edge == BOTTOM )
	{
		MoveTo( frame.left, screen.bottom - frame.Height() );
		frame = Frame();
	}
}

void
PanelWindow::WorkspaceActivated( int32, bool active )
{
	if ( active )
	{
		MoveTo( m_pos );
		
		BScreen screen(this);
		
		ScreenChanged( screen.Frame(), B_RGBA32 );
	}
}

bool
PanelWindow::IsExpandOnMouseOver()
{
	return m_expand_on_mouse_over;
}

bool
PanelWindow::IsOpen()
{
	return m_open;
}

bigtime_t
PanelWindow::AnimationDelay()
{
	return m_pulse_rate;
}
