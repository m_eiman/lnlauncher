#ifndef BUBBLE_WINDOW_H
#define BUBBLE_WINDOW_H

#include <Window.h>
#include <View.h>
#include <String.h>


class BubbleView : public BView {
	public:
		BubbleView( const char * text );
		virtual ~BubbleView();
		
		virtual void MouseMoved( BPoint, uint32, const BMessage * );
		virtual void AttachedToWindow();
		
		virtual void Draw( BRect );

	protected:
		BString			m_text;
		font_height		m_fh;
		int				m_moves;
	};


class BubbleWindow : public BWindow {
	public:
		BubbleWindow( BPoint where, const char * text );
		virtual ~BubbleWindow();
	};


enum {
	BUBBLE_TEXT_REQUEST = 'btRQ',
	BUBBLE_TEXT_REPLY	= 'btRP'	// add a string named "text"
	};


class BubbleStarter : public BView {
	public:
		BubbleStarter();
		BubbleStarter( BMessage * );
		virtual ~BubbleStarter();
		
		static BArchivable * Instantiate( BMessage * );
		
		virtual void AttachedToWindow();
		
		virtual void MouseMoved( BPoint, uint32, const BMessage * );
		virtual void MessageReceived( BMessage * );
		
		virtual void Pulse();
		
	private:
		BPoint		m_curr_pos, m_last_pos;
		BString		m_last_text;
		BView		* m_last_view;
	};

#endif

